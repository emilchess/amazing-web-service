# -*- coding: utf-8 -*-
from collections import namedtuple
from datetime import datetime
import pytz


Route = namedtuple('Route', ('price', 'duration', 'hops'))


def get_price(price_root):
    return {elem.get('type'): float(elem.text)
            for elem in price_root.findall('ServiceCharges')
            if elem.get('ChargeType') == 'TotalAmount'}


def get_hop(hop_root):
    hop = {}
    for elem in hop_root:
        if elem.tag == 'Source':
            hop['source'] = elem.text
        elif elem.tag == 'Destination':
            hop['destination'] = elem.text
        elif elem.tag == 'DepartureTimeStamp':
            hop['departure_time'] = datetime.strptime(elem.text, '%Y-%m-%dT%H%M')
        elif elem.tag == 'ArrivalTimeStamp':
            hop['arrival_time'] = datetime.strptime(elem.text, '%Y-%m-%dT%H%M')
    return hop


def get_flights(root):
    flights_data = root.findall('PricedItineraries/Flights')
    flights = []
    for flight in flights_data:
        hops = [get_hop(hop_data)
                for hop_data in flight.findall('OnwardPricedItinerary/Flights/Flight')]
        price = get_price(flight.find('Pricing'))
        flights.append((hops, price))
    return flights


def datetime_to_utc(local_dt, timezone_string):
    timezone = pytz.timezone(timezone_string)
    return timezone.localize(local_dt).astimezone(pytz.utc)


def get_routes(root, source, destination, iata_dict, ticket_type='SingleAdult'):
    flights = get_flights(root)
    routes = []
    for hops, price in flights:
        if hops:
            first_hop = hops[0]
            last_hop = hops[-1]
            if first_hop['source'] == source and last_hop['destination'] == destination:
                source_timezone = iata_dict.get(first_hop['source'])
                destination_timezone = iata_dict.get(last_hop['destination'])
                routes.append(Route(price[ticket_type],
                                    datetime_to_utc(last_hop['arrival_time'], source_timezone) -
                                    datetime_to_utc(first_hop['departure_time'], destination_timezone),
                                    hops))
    return routes


def get_cheapest_route(routes):
    return sorted(routes, key=lambda route: route.price)[0]


def get_quickest_route(routes):
    return sorted(routes, key=lambda route: route.duration)[0]


def get_optimal_route(routes):
    return sorted(routes, key=lambda route: route.price * route.duration)[0]
