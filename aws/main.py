# -*- coding: utf-8 -*-
import json
import xml.etree.ElementTree as ET
from functools import partial
from aiohttp import web
from flights import (get_routes,
                     get_cheapest_route, get_quickest_route, get_optimal_route)


async def ping_handler(request):
    _ = request
    return web.json_response({
        'result': 'pong'
    })


async def routes_handler(request):
    response = json.dumps(request.app['routes'], sort_keys=True, default=str)
    return web.Response(text=response)


async def best_routes_handler(request):
    routes = request.app['routes']
    return web.json_response({
        'cheapest': get_cheapest_route(routes),
        'quickest': get_quickest_route(routes),
        'best': get_optimal_route(routes)
    }, dumps=partial(json.dumps, indent=4, sort_keys=True, default=str))


def read_iata_file():
    result = {}
    with open('files/iata.tzmap') as iata_file:
        for line in iata_file.readlines():
            iata_code, tz_string = line.split(' ')
            result[iata_code] = tz_string.strip()
    return result


async def on_startup(app):
    app['iata_map'] = read_iata_file()
    app['routes'] = (get_routes(ET.parse('files/RS_Via-3.xml'), app['source'], app['destination'], app['iata_map'])
                     + get_routes(ET.parse('files/RS_ViaOW.xml'), app['source'], app['destination'], app['iata_map']))


def init_app():
    app = web.Application()
    app['source'] = 'DXB'
    app['destination'] = 'BKK'

    app.on_startup.append(on_startup)
    app.add_routes([
        web.get('/ping', ping_handler),
        web.get('/routes', routes_handler),
        web.get('/best_routes', best_routes_handler)
    ])
    return app


web.run_app(init_app())
