# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
from datetime import datetime, timedelta
import pytest
import pytz
from aws.flights import (get_price, get_hop, get_flights,
                         datetime_to_utc, get_routes,
                         get_cheapest_route, get_quickest_route, get_optimal_route,
                         Route)


def test_get_price():
    root = ET.fromstring('''<Pricing currency="SGD">
                <ServiceCharges type="SingleAdult" ChargeType="BaseFare">272.00</ServiceCharges>
                <ServiceCharges type="SingleAdult" ChargeType="AirlineTaxes">169.80</ServiceCharges>
                <ServiceCharges type="SingleAdult" ChargeType="TotalAmount">441.80</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="BaseFare">237.00</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="AirlineTaxes">169.80</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="TotalAmount">406.80</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="BaseFare">28.00</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="AirlineTaxes">59.90</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="TotalAmount">87.90</ServiceCharges>
            </Pricing>''')
    assert get_price(root) == {
        'SingleAdult': 441.8,
        'SingleChild': 406.8,
        'SingleInfant': 87.9
    }


def test_get_hop():
    root = ET.fromstring('''<Flight>
                        <Carrier id="9W">JetAirways</Carrier>
                        <FlightNumber>537</FlightNumber>
                        <Source>DXB</Source>
                        <Destination>BOM</Destination>
                        <DepartureTimeStamp>2018-10-27T1855</DepartureTimeStamp>
                        <ArrivalTimeStamp>2018-10-27T2325</ArrivalTimeStamp>
                        <Class>K</Class>
                        <NumberOfStops>0</NumberOfStops>
                        <FareBasis>
2820303decf751-5511-447a-aeb1-810a6b10ad7d@@$255_DXB_BOM_537_6_18:55_$255_BOM_BKK_70_6_09:15__A2_1_1
</FareBasis>
                        <WarningText/>
                        <TicketType>E</TicketType>
                        </Flight>''')
    assert get_hop(root) == {
        'source': 'DXB',
        'destination': 'BOM',
        'departure_time': datetime(2018, 10, 27, 18, 55),
        'arrival_time': datetime(2018, 10, 27, 23, 25)
    }


@pytest.mark.parametrize('local_dt,tz_string,expected', [
    (datetime(2018, 10, 27, 18, 55), 'Europe/Moscow', datetime(2018, 10, 27, 15, 55, tzinfo=pytz.utc)),
])
def test_datetime_to_utc(local_dt, tz_string, expected):
    assert datetime_to_utc(local_dt, tz_string) == expected


def test_get_flights():
    root = ET.fromstring('''
        <AirFareSearchResponse>
        <PricedItineraries>
        <Flights>
            <OnwardPricedItinerary>
                <Flights>
                    <Flight>
                        <Carrier id="AI">AirIndia</Carrier>
                        <FlightNumber>996</FlightNumber>
                        <Source>DXB</Source>
                        <Destination>DEL</Destination>
                        <DepartureTimeStamp>2018-10-22T0005</DepartureTimeStamp>
                        <ArrivalTimeStamp>2018-10-22T0445</ArrivalTimeStamp>
                        <Class>G</Class>
                        <NumberOfStops>0</NumberOfStops>
                        <FareBasis>
                            2820231f40c802-03e6-4655-9ece-0fb1ad670b5c@@$255_DXB_DEL_996_9_00:05_$255_DEL_BKK_332_9_13:50_$255_BKK_DEL_333_9_08:50_$255_DEL_DXB_995_9_20:40__A2_0_0
                        </FareBasis>
                        <WarningText/>
                        <TicketType>E</TicketType>
                    </Flight>
                    <Flight>
                        <Carrier id="AI">AirIndia</Carrier>
                        <FlightNumber>332</FlightNumber>
                        <Source>DEL</Source>
                        <Destination>BKK</Destination>
                        <DepartureTimeStamp>2018-10-22T1350</DepartureTimeStamp>
                        <ArrivalTimeStamp>2018-10-22T1935</ArrivalTimeStamp>
                        <Class>G</Class>
                        <NumberOfStops>0</NumberOfStops>
                        <FareBasis>
                            2820231f40c802-03e6-4655-9ece-0fb1ad670b5c@@$255_DXB_DEL_996_9_00:05_$255_DEL_BKK_332_9_13:50_$255_BKK_DEL_333_9_08:50_$255_DEL_DXB_995_9_20:40__A2_0_0
                        </FareBasis>
                        <WarningText/>
                        <TicketType>E</TicketType>
                    </Flight>
                </Flights>
            </OnwardPricedItinerary>
            <Pricing currency="SGD">
                <ServiceCharges type="SingleAdult" ChargeType="BaseFare">233.00</ServiceCharges>
                <ServiceCharges type="SingleAdult" ChargeType="AirlineTaxes">152.40</ServiceCharges>
                <ServiceCharges type="SingleAdult" ChargeType="TotalAmount">385.40</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="BaseFare">233.00</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="AirlineTaxes">132.20</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="TotalAmount">365.20</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="BaseFare">129.00</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="AirlineTaxes">11.40</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="TotalAmount">140.40</ServiceCharges>
            </Pricing>
        </Flights>
        </PricedItineraries>
        </AirFareSearchResponse>''')
    flights = get_flights(root)
    assert len(flights) == 1
    hops, price = flights[0]
    assert len(hops) == 2
    delhi, bangkok = hops
    assert delhi == {
        'source': 'DXB',
        'destination': 'DEL',
        'departure_time': datetime(2018, 10, 22, 0, 5),
        'arrival_time': datetime(2018, 10, 22, 4, 45)
    }
    assert bangkok == {
        'source': 'DEL',
        'destination': 'BKK',
        'departure_time': datetime(2018, 10, 22, 13, 50),
        'arrival_time': datetime(2018, 10, 22, 19, 35)
    }
    assert price == {
        'SingleAdult': 385.4,
        'SingleChild': 365.2,
        'SingleInfant': 140.4
    }


def test_get_routes():
    root = ET.fromstring('''
        <AirFareSearchResponse>
        <PricedItineraries>
        <Flights>
            <OnwardPricedItinerary>
                <Flights>
                    <Flight>
                        <Carrier id="AI">AirIndia</Carrier>
                        <FlightNumber>996</FlightNumber>
                        <Source>DXB</Source>
                        <Destination>DEL</Destination>
                        <DepartureTimeStamp>2018-10-22T0005</DepartureTimeStamp>
                        <ArrivalTimeStamp>2018-10-22T0445</ArrivalTimeStamp>
                        <Class>G</Class>
                        <NumberOfStops>0</NumberOfStops>
                        <FareBasis>
                            2820231f40c802-03e6-4655-9ece-0fb1ad670b5c@@$255_DXB_DEL_996_9_00:05_$255_DEL_BKK_332_9_13:50_$255_BKK_DEL_333_9_08:50_$255_DEL_DXB_995_9_20:40__A2_0_0
                        </FareBasis>
                        <WarningText/>
                        <TicketType>E</TicketType>
                    </Flight>
                    <Flight>
                        <Carrier id="AI">AirIndia</Carrier>
                        <FlightNumber>332</FlightNumber>
                        <Source>DEL</Source>
                        <Destination>BKK</Destination>
                        <DepartureTimeStamp>2018-10-22T1350</DepartureTimeStamp>
                        <ArrivalTimeStamp>2018-10-22T1935</ArrivalTimeStamp>
                        <Class>G</Class>
                        <NumberOfStops>0</NumberOfStops>
                        <FareBasis>
                            2820231f40c802-03e6-4655-9ece-0fb1ad670b5c@@$255_DXB_DEL_996_9_00:05_$255_DEL_BKK_332_9_13:50_$255_BKK_DEL_333_9_08:50_$255_DEL_DXB_995_9_20:40__A2_0_0
                        </FareBasis>
                        <WarningText/>
                        <TicketType>E</TicketType>
                    </Flight>
                </Flights>
            </OnwardPricedItinerary>
            <Pricing currency="SGD">
                <ServiceCharges type="SingleAdult" ChargeType="BaseFare">233.00</ServiceCharges>
                <ServiceCharges type="SingleAdult" ChargeType="AirlineTaxes">152.40</ServiceCharges>
                <ServiceCharges type="SingleAdult" ChargeType="TotalAmount">385.40</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="BaseFare">233.00</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="AirlineTaxes">132.20</ServiceCharges>
                <ServiceCharges type="SingleChild" ChargeType="TotalAmount">365.20</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="BaseFare">129.00</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="AirlineTaxes">11.40</ServiceCharges>
                <ServiceCharges type="SingleInfant" ChargeType="TotalAmount">140.40</ServiceCharges>
            </Pricing>
        </Flights>
        </PricedItineraries>
        </AirFareSearchResponse>''')
    iata_dict = {
        'DXB': 'Asia/Dubai',
        'DEL': 'Asia/Kolkata',
        'BKK': 'Asia/Bangkok',
    }
    routes = get_routes(root, 'DXB', 'BKK', iata_dict)
    assert len(routes) == 1
    route = routes[0]
    assert route.price == 385.4
    assert route.duration == timedelta(hours=22.5)


def test_get_cheapest_route():
    routes = [
        Route(101, timedelta(hours=2), []),
        Route(100, timedelta(hours=1.5), []),
        Route(102, timedelta(hours=1), [])
    ]
    assert get_cheapest_route(routes) == Route(100, timedelta(hours=1.5), [])


def test_get_quickest_route():
    routes = [
        Route(101, timedelta(hours=2), []),
        Route(100, timedelta(hours=1.5), []),
        Route(102, timedelta(hours=1), [])
    ]
    assert get_quickest_route(routes) == Route(102, timedelta(hours=1), [])


def test_get_optimal_route():
    routes = [
        Route(150, timedelta(hours=1.1), []),
        Route(100, timedelta(hours=1.5), []),
        Route(200, timedelta(hours=1), [])
    ]
    assert get_optimal_route(routes) == Route(100, timedelta(hours=1.5), [])
