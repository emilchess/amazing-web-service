# Amazing web service

Find your way to BKK

## Setup
`pip install -r requirements.txt`

##  Run

`python3 aws/main.py`

## Test 
`curl localhost:8080/best_routes`

```
{
    "best": 
    [
        385.4,
        "18:30:00",
        [
            {
                "arrival_time": "2018-10-27 12:25:00",
                "departure_time": "2018-10-27 01:40:00",
                "destination": "CAN",
                "source": "DXB"
            },
            {
                "arrival_time": "2018-10-27 17:10:00",
                "departure_time": "2018-10-27 14:50:00",
                "destination": "BKK",
                "source": "CAN"
            }
        ]
    ],
    "cheapest": [
        385.4,
        "22:30:00",
        [
            {
                "arrival_time": "2018-10-22 04:45:00",
                "departure_time": "2018-10-22 00:05:00",
                "destination": "DEL",
                "source": "DXB"
            },
            {
                "arrival_time": "2018-10-22 19:35:00",
                "departure_time": "2018-10-22 13:50:00",
                "destination": "BKK",
                "source": "DEL"
            }
        ]

    ],
    "quickest": [
        385.4,
        "18:30:00",
    [
        {

            "arrival_time": "2018-10-27 12:25:00",
            "departure_time": "2018-10-27 01:40:00",
            "destination": "CAN",
            "source": "DXB"

        },
        {
            "arrival_time": "2018-10-27 17:10:00",
            "departure_time": "2018-10-27 14:50:00",
            "destination": "BKK",
            "source": "CAN"
        }
    ]
]
```
# TODO

1. Для определения оптимального маршрута дополнительно использовать:
- Рейтинг авиаперевозчиков
- Класс билета
- Количество хопов

2. Добавить тесты на методы API
